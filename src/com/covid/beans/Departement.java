package com.covid.beans;

import java.util.*;

/** @pdOid 1ceefff4-adf3-4a44-991d-3298bbcc2d59 */
/**
 * 
 * @author Maestros
 *
 */
public class Departement {
   /** @pdOid aab0b86e-424c-40c0-881e-34cf9b4976ea */
   private int id;
   /** @pdOid 6513a660-93eb-4132-979d-039d2bdcd980 */
   private String nom;
   
   public java.util.Collection Association_7;
   /** @pdRoleInfo migr=no name=Region assc=Association_2 coll=java.util.Collection impl=java.util.HashSet mult=1..1 */
   public Region region;
   
   public Departement() {}

public Departement(int id, String nom, Collection association_7, Region region) {
	super();
	this.id = id;
	this.nom = nom;
	Association_7 = association_7;
	this.region = region;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public java.util.Collection getAssociation_7() {
	return Association_7;
}

public void setAssociation_7(java.util.Collection association_7) {
	Association_7 = association_7;
}

public Region getRegion() {
	return region;
}

public void setRegion(Region region) {
	this.region = region;
}
   
   

}