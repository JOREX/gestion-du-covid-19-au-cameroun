package com.covid.beans;

import java.util.*;

/** @pdOid 5c46ca93-db69-43f7-9206-1f64b0321a64 */
public class Menage {
   /** @pdOid 8df3a84c-6a4b-4bde-8b02-f0ce2e8e3d23 */
   private long id;
   /** @pdOid f33b9101-93da-4e98-acf2-a26fe7cc0fef */
   private String name;
   /** @pdOid 8f3c400c-ca9e-443b-a6c0-dff67b8b025d */
   private int nombreMembre;
   /** @pdOid 71e17fe4-7bca-44da-96e5-a52d026dd7da */
   private String localisation;
   
   /** @pdRoleInfo migr=no name=Agent assc=Association_13 coll=java.util.Collection impl=java.util.HashSet mult=0..1 type=Aggregation */
   public Agent agent;
   public java.util.Collection citoyens;
   
   
   public Menage() {}


public Menage(long id, String name, int nombreMembre, String localisation, Agent agent, Collection citoyens) {
	super();
	this.id = id;
	this.name = name;
	this.nombreMembre = nombreMembre;
	this.localisation = localisation;
	this.agent = agent;
	this.citoyens = citoyens;
}


public long getId() {
	return id;
}


public void setId(long id) {
	this.id = id;
}


public String getName() {
	return name;
}


public void setName(String name) {
	this.name = name;
}


public int getNombreMembre() {
	return nombreMembre;
}


public void setNombreMembre(int nombreMembre) {
	this.nombreMembre = nombreMembre;
}


public String getLocalisation() {
	return localisation;
}


public void setLocalisation(String localisation) {
	this.localisation = localisation;
}


public Agent getAgent() {
	return agent;
}


public void setAgent(Agent agent) {
	this.agent = agent;
}


public java.util.Collection getCitoyens() {
	return citoyens;
}


public void setCitoyens(java.util.Collection citoyens) {
	this.citoyens = citoyens;
}
   
   
   

}