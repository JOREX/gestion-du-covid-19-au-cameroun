package com.covid.beans;

import java.util.*;

/** @pdOid 1621f21b-83a1-4db6-99bb-b1c3023ecf6a */
public class Agent extends Citoyen {
   /** @pdOid 9e640f91-2516-4183-a325-c92136fdf435 */
   private long id;
   /** @pdOid 2f03eac5-342a-4dd8-86ff-a3ff7e73cd35 */
   private String typeAgent;
   
   /** @pdRoleInfo migr=no name=Symptome assc=association15 coll=java.util.Collection impl=java.util.HashSet mult=0..* type=Aggregation */
   public java.util.Collection<Symptome> symptome;
   /** @pdRoleInfo migr=no name=Secteur assc=Association_4 mult=1..1 */
   public Secteur secteur;
   
   
   public Agent() {}
   
   
   
   public long getId() {
	return id;
}



public void setId(int id) {
	this.id = id;
}



public String getTypeAgent() {
	return typeAgent;
}



public void setTypeAgent(String typeAgent) {
	this.typeAgent = typeAgent;
}



public Secteur getSecteur() {
	return secteur;
}



public void setSecteur(Secteur secteur) {
	this.secteur = secteur;
}



/** @pdGenerated default getter */
   public java.util.Collection<Symptome> getSymptome() {
      if (symptome == null)
         symptome = new java.util.HashSet<Symptome>();
      return symptome;
   }
   
   /** @pdGenerated default iterator getter */
   public java.util.Iterator getIteratorSymptome() {
      if (symptome == null)
         symptome = new java.util.HashSet<Symptome>();
      return symptome.iterator();
   }
   
   /** @pdGenerated default setter
     * @param newSymptome */
   public void setSymptome(java.util.Collection<Symptome> newSymptome) {
      removeAllSymptome();
      for (java.util.Iterator iter = newSymptome.iterator(); iter.hasNext();)
         addSymptome((Symptome)iter.next());
   }
   
   /** @pdGenerated default add
     * @param newSymptome */
   public void addSymptome(Symptome newSymptome) {
      if (newSymptome == null)
         return;
      if (this.symptome == null)
         this.symptome = new java.util.HashSet<Symptome>();
      if (!this.symptome.contains(newSymptome))
         this.symptome.add(newSymptome);
   }
   
   /** @pdGenerated default remove
     * @param oldSymptome */
   public void removeSymptome(Symptome oldSymptome) {
      if (oldSymptome == null)
         return;
      if (this.symptome != null)
         if (this.symptome.contains(oldSymptome))
            this.symptome.remove(oldSymptome);
   }
   
   /** @pdGenerated default removeAll */
   public void removeAllSymptome() {
      if (symptome != null)
         symptome.clear();
   }

}