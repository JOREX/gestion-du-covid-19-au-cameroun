package com.covid.beans;

import java.util.*;

/** @param <Citoyen>
 * @pdOid d4ef5ee7-3488-4e6a-bdce-9e7faa95ad3c */
public class Voyage<Citoyen> {
   /** @pdOid 697770c3-de5c-44b6-a471-ed8a97d8b188 */
   private long id;
   /** @pdOid 28f70b44-a287-4e67-b3c6-fec403849374 */
   private String date;
   /** @pdOid 834fc7b3-3ee1-46e1-89ed-329b580924b8 */
   private String lieu;
   
   /** @pdRoleInfo migr=no name=Citoyen assc=Association_9 coll=java.util.Collection impl=java.util.HashSet mult=1..1 */
   private Citoyen citoyen;
   
   public Voyage() {}

public Voyage(int id, String date, String lieu, Citoyen citoyen) {
	super();
	this.id = id;
	this.date = date;
	this.lieu = lieu;
	this.citoyen = citoyen;
}

public long getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getDate() {
	return date;
}

public void setDate(String date) {
	this.date = date;
}

public String getLieu() {
	return lieu;
}

public void setLieu(String lieu) {
	this.lieu = lieu;
}

public Citoyen getCitoyen() {
	return citoyen;
}

public void setCitoyen(Citoyen citoyen) {
	this.citoyen = citoyen;
}
   
   
   
   

}