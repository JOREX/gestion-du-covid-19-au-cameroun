package com.covid.beans;

import java.util.*;

/** @pdOid ae081c3d-1df7-4be5-9ccf-a8dec7a5239e */
public class Secteur {
   /** @pdOid 6f57a5f2-929f-4301-ac97-dc9bb0fd7434 */
   private long id;
   /** @pdOid 2f702e8a-1c67-4640-b24a-5f48e87a3fab */
   private String nom;
   
   /** @pdRoleInfo migr=no name=Menage assc=association13 mult=1..1 type=Composition */
 
   /** @pdRoleInfo migr=no name=Departement assc=Association_3 coll=java.util.Collection impl=java.util.HashSet mult=1..1 */
   public Departement departement;
   
   ArrayList<Menage> menages;
   ArrayList<Agent> agents;
   ArrayList<Denree> denrees;

   public Secteur() {}

public Secteur(int id, String nom, Departement departement) {
	super();
	this.id = id;
	this.nom = nom;
	this.departement = departement;
}


public ArrayList<Menage> getMenages() {
	return menages;
}

public void setMenages(ArrayList<Menage> menages) {
	this.menages = menages;
}

public ArrayList<Agent> getAgents() {
	return agents;
}

public void setAgents(ArrayList<Agent> agents) {
	this.agents = agents;
}

public ArrayList<Denree> getDenrees() {
	return denrees;
}

public void setDenrees(ArrayList<Denree> denrees) {
	this.denrees = denrees;
}

public long getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public Departement getDepartement() {
	return departement;
}

public void setDepartement(Departement departement) {
	this.departement = departement;
}
   
   
}