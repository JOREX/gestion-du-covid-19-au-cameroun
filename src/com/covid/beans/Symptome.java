package com.covid.beans;

import java.util.*;

/** @param <Citoyen>
 * @pdOid e4f75a58-e457-4d75-89c0-5afe0f062a30 */
public class Symptome<Citoyen> {
   /** @pdOid 56ca9aa6-829e-41b8-8e77-4267c7ba89c1 */
   private long id;
   /** @pdOid c9bb19d0-c972-4ca1-9e01-c71d209dbd4e */
   private String libelle;
   /** @pdOid fb3cc433-6368-48ec-bc34-d9ebededdaca */
   private int description;
   
   private Citoyen citoyen;
   
   public Symptome() {}

public Symptome(int id, String libelle, int description, Citoyen citoyen) {
	super();
	this.id = id;
	this.libelle = libelle;
	this.description = description;
	this.citoyen = citoyen;
}

public long getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getLibelle() {
	return libelle;
}

public void setLibelle(String libelle) {
	this.libelle = libelle;
}

public int getDescription() {
	return description;
}

public void setDescription(int description) {
	this.description = description;
}

public Citoyen getCitoyen() {
	return citoyen;
}

public void setCitoyen(Citoyen citoyen) {
	this.citoyen = citoyen;
}
   
   

}