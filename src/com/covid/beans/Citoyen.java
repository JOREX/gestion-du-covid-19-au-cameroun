package com.covid.beans;

import java.util.*;

/** @pdOid a34327b4-164d-4926-a049-4afd49b1a2e8 */
public class Citoyen {
	/** @pdOid 976ae93f-34fc-4089-92a6-a6c6301d12f9 */
	protected long id;
	/** @pdOid 6d513e67-6d93-4ba0-9e6d-3af055bbf6cf */
	protected String nom;
	/** @pdOid 48a18f78-b5ca-42a6-bd1f-df3442b35e69 */
	protected String prenom;

	/** @pdOid afa8ec58-97f2-4890-bc05-72df7438c5b4 */
	protected String dateNaissance;
	/** @pdOid 2e55ace1-91ea-4e2c-a1d2-9dbddfe4271b */
	protected String sexe;
	/** @pdOid bfe12e4d-0236-4466-b93b-ad4c02045364 */
	protected String image;
	/** @pdOid 3369ccc3-a605-47f3-8083-4791c9e86afe */
	protected String profession;
	/** @pdOid 5b4bafdb-1212-4307-b293-218c6cf4e4ff */
	protected boolean estContamine;

	/**
	 * @pdRoleInfo migr=no name=Agent assc=Association_12 coll=java.util.Collection
	 *             impl=java.util.HashSet mult=0..1
	 */
	protected Agent agent;
	/**
	 * @pdRoleInfo migr=no name=Symptome assc=association14
	 *             coll=java.util.Collection impl=java.util.HashSet mult=0..*
	 *             type=Composition
	 */
	protected java.util.Collection<Symptome> symptome;
	/**
	 * @pdRoleInfo migr=no name=Citoyen assc=Association_10
	 *             coll=java.util.Collection impl=java.util.HashSet mult=0..*
	 */
	protected java.util.Collection<Citoyen> citoyenB;
	/**
	 * @pdRoleInfo migr=no name=Menage assc=Association_11 coll=java.util.Collection
	 *             impl=java.util.HashSet mult=1..1
	 */
	protected Menage menage;

	public Citoyen() {

	}

	/** @pdGenerated default getter */
	public java.util.Collection<Symptome> getSymptome() {
		if (symptome == null)
			symptome = new java.util.HashSet<Symptome>();
		return symptome;
	}

	public long getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getDateNaissance() {
		return dateNaissance;
	}

	public void setDateNaissance(String dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getSexe() {
		return sexe;
	}

	public void setSexe(String sexe) {
		this.sexe = sexe;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getProfession() {
		return profession;
	}

	public void setProfession(String profession) {
		this.profession = profession;
	}

	public boolean isEstContamine() {
		return estContamine;
	}

	public void setEstContamine(boolean estContamine) {
		this.estContamine = estContamine;
	}

	public Agent getAgent() {
		return agent;
	}

	public void setAgent(Agent agent) {
		this.agent = agent;
	}

	public Menage getMenage() {
		return menage;
	}

	public void setMenage(Menage menage) {
		this.menage = menage;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator getIteratorSymptome() {
		if (symptome == null)
			symptome = new java.util.HashSet<Symptome>();
		return symptome.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newSymptome
	 */
	public void setSymptome(java.util.Collection<Symptome> newSymptome) {
		removeAllSymptome();
		for (java.util.Iterator iter = newSymptome.iterator(); iter.hasNext();)
			addSymptome((Symptome) iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newSymptome
	 */
	public void addSymptome(Symptome newSymptome) {
		if (newSymptome == null)
			return;
		if (this.symptome == null)
			this.symptome = new java.util.HashSet<Symptome>();
		if (!this.symptome.contains(newSymptome))
			this.symptome.add(newSymptome);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldSymptome
	 */
	public void removeSymptome(Symptome oldSymptome) {
		if (oldSymptome == null)
			return;
		if (this.symptome != null)
			if (this.symptome.contains(oldSymptome))
				this.symptome.remove(oldSymptome);
	}

	/** @pdGenerated default removeAll */
	public void removeAllSymptome() {
		if (symptome != null)
			symptome.clear();
	}

	/** @pdGenerated default getter */
	public java.util.Collection<Citoyen> getCitoyenB() {
		if (citoyenB == null)
			citoyenB = new java.util.HashSet<Citoyen>();
		return citoyenB;
	}

	/** @pdGenerated default iterator getter */
	public java.util.Iterator getIteratorCitoyenB() {
		if (citoyenB == null)
			citoyenB = new java.util.HashSet<Citoyen>();
		return citoyenB.iterator();
	}

	/**
	 * @pdGenerated default setter
	 * @param newCitoyenB
	 */
	public void setCitoyenB(java.util.Collection<Citoyen> newCitoyenB) {
		removeAllCitoyenB();
		for (java.util.Iterator iter = newCitoyenB.iterator(); iter.hasNext();)
			addCitoyenB((Citoyen) iter.next());
	}

	/**
	 * @pdGenerated default add
	 * @param newCitoyen
	 */
	public void addCitoyenB(Citoyen newCitoyen) {
		if (newCitoyen == null)
			return;
		if (this.citoyenB == null)
			this.citoyenB = new java.util.HashSet<Citoyen>();
		if (!this.citoyenB.contains(newCitoyen))
			this.citoyenB.add(newCitoyen);
	}

	/**
	 * @pdGenerated default remove
	 * @param oldCitoyen
	 */
	public void removeCitoyenB(Citoyen oldCitoyen) {
		if (oldCitoyen == null)
			return;
		if (this.citoyenB != null)
			if (this.citoyenB.contains(oldCitoyen))
				this.citoyenB.remove(oldCitoyen);
	}

	/** @pdGenerated default removeAll */
	public void removeAllCitoyenB() {
		if (citoyenB != null)
			citoyenB.clear();
	}

}