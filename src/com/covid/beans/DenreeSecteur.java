package com.covid.beans;

import java.util.*;

/** @pdOid 65f59a6a-b5da-4f59-b27e-b3be4c8d6716 */
/**
 * 
 * @author Maestros
 *
 */
public class DenreeSecteur {
   /** @pdOid b18a663f-5f6e-4df8-8a20-e55b5cca6871 */
   private int quantite;
   
   private Secteur secteurB;
   
   private Denree denree;
   
   public DenreeSecteur() {}

public DenreeSecteur(int quantite, Secteur secteurB, Denree denree) {
	super();
	this.quantite = quantite;
	this.secteurB = secteurB;
	this.denree = denree;
}

public int getQuantite() {
	return quantite;
}

public void setQuantite(int quantite) {
	this.quantite = quantite;
}

public Secteur getSecteurB() {
	return secteurB;
}

public void setSecteurB(Secteur secteurB) {
	this.secteurB = secteurB;
}

public Denree getDenree() {
	return denree;
}

public void setDenree(Denree denree) {
	this.denree = denree;
}
   
   
   
   

}