package com.covid.beans;

import java.util.*;

/** @pdOid 745c87b1-6513-4134-8a25-f8d1cf6d51b2 */
public class Region {
   /** @pdOid a258a84d-d5ca-4cce-8036-d1c55e993862 */
   private int id;
   /** @pdOid 29493dda-5d47-46df-89f2-e2d5c1634ca8 */
   private String nom;
   
   public java.util.Collection departements;
   
   public Region() {}

public Region(int id, String nom, Collection departements) {
	super();
	this.id = id;
	this.nom = nom;
	this.departements = departements;
}

public int getId() {
	return id;
}

public void setId(int id) {
	this.id = id;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}

public java.util.Collection getDepartements() {
	return departements;
}

public void setDepartements(java.util.Collection departements) {
	this.departements = departements;
}
   
   

}