DROP DATABASE IF EXISTS `covid_19_management`;
CREATE DATABASE `covid_19_management` CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `covid_19_management`;


CREATE TABLE `region`
(
	`id` INTEGER NOT NULL  AUTO_INCREMENT, 
	`nom` VARCHAR(255) NOT NULL, 
	CONSTRAINT pk_region PRIMARY KEY(id) 
)
ENGINE=InnoDB 
CHARACTER SET 
utf8 COLLATE utf8_general_ci;


CREATE TABLE `departement`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT, 
	`nom` VARCHAR(255) NOT NULL, 
	`id_region` INTEGER NOT NULL, 
	CONSTRAINT pk_departement PRIMARY KEY (id), 
	CONSTRAINT fk_departement_region FOREIGN KEY (id_region) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE
) 
ENGINE=InnoDB 
CHARACTER SET utf8 
COLLATE utf8_general_ci;


CREATE TABLE `secteur`
(
	 `id` INTEGER NOT NULL AUTO_INCREMENT, 
	 `nom` VARCHAR(255) NOT NULL, 
	 `id_deplacement` INTEGER NOT NULL, 
	 CONSTRAINT pk_secteur PRIMARY KEY(id), 
	 CONSTRAINT fk_secteur_departement FOREIGN KEY (id_deplacement) REFERENCES departement(id) ON DELETE CASCADE ON UPDATE CASCADE
) 
ENGINE=InnoDB 
CHARACTER SET utf8 
COLLATE utf8_general_ci;


CREATE TABLE `agent`
(
	
	`id` INTEGER NOT NULL AUTO_INCREMENT, 
	`login` VARCHAR(255) UNIQUE NOT NULL,
	`password` VARCHAR(500) NOT NULL,
	`nom` VARCHAR(255) NOT NULL, 
	`prenom` VARCHAR(255),
	`date_naissance` DATE NOT NULL, 
	`sexe` VARCHAR(80) NOT NULL, 
	`image` VARCHAR(255), 
	`profession` VARCHAR(255), 
	`est_contamine` BOOLEAN DEFAULT false, 
	`id_secteur` INTEGER NOT NULL, 
	CONSTRAINT pk_agent PRIMARY KEY(id),  
	CONSTRAINT fk_agent_secteur FOREIGN KEY (id_secteur) REFERENCES secteur(id) ON DELETE CASCADE ON UPDATE CASCADE
)  
ENGINE=InnoDB 
CHARACTER SET utf8 
COLLATE utf8_general_ci;


CREATE TABLE `menage`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`nom` VARCHAR(255) NOT NULL,
	`nombre_membre` INTEGER NOT NULL,
	`localisation` VARCHAR(255),
	`id_secteur` INTEGER NOT NULL,
	`id_agent` INTEGER NOT NULL,
	CONSTRAINT pk_menage PRIMARY KEY(id),
	CONSTRAINT fk_menage_secteur FOREIGN KEY (id_secteur) REFERENCES secteur(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_menage_agent FOREIGN KEY (id_agent) REFERENCES agent(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `citoyen`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`login` VARCHAR(255) UNIQUE NOT NULL,
	`password` VARCHAR(500) NOT NULL,	
	`nom` VARCHAR(255) NOT NULL,
	`prenom` VARCHAR(255) NOT NULL,
	`date_naissance` Date NOT NULL,
	`sexe` VARCHAR(80) NOT NULL,
	`image` VARCHAR(255),
	`profession` VARCHAR(255),
	`est_contamine` BOOLEAN,
	`id_agent` INTEGER NOT NULL,
	`id_menage` INTEGER NOT NULL,

	CONSTRAINT pk_citoyen PRIMARY KEY(id),
	CONSTRAINT fk_citoyen_agent FOREIGN KEY (id_agent) REFERENCES agent(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_citoyen_menage FOREIGN KEY (id_menage) REFERENCES menage(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


CREATE TABLE `symptome`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`libelle` VARCHAR(255) NOT NULL,
	`description` TEXT NOT NULL,
	`id_agent` INTEGER NOT NULL,
	`id_citoyen` INTEGER NOT NULL,

	CONSTRAINT pk_symptome PRIMARY KEY(id),
	CONSTRAINT fk_symptome_agent FOREIGN KEY (id_agent) REFERENCES agent(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_symptome_citoyen FOREIGN KEY (id_citoyen) REFERENCES citoyen(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `contamination`
(
	`id_contamine` INTEGER NOT NULL,
	`id_contagieux` INTEGER NOT NULL,

	CONSTRAINT pk_contamination PRIMARY KEY(id_contamine,id_contagieux),
	CONSTRAINT fk_contamination1_citoyen FOREIGN KEY (id_contamine) REFERENCES citoyen(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_contamination2_citoyen FOREIGN KEY (id_contagieux) REFERENCES citoyen(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `voyage`
(
	`id` INTEGER NOT NULL AUTO_INCREMENT,
	`date` DATE NOT NULL,
	`lieu` VARCHAR(255),
	`id_citoyen` INTEGER NOT NULL,


	CONSTRAINT pk_voyage PRIMARY KEY(id),
	CONSTRAINT fk_symptome_citoyens FOREIGN KEY (id_citoyen) REFERENCES citoyen(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;


CREATE TABLE `denree`
(
	`id` INTEGER NOT NULL  AUTO_INCREMENT,
	`libelle` VARCHAR(255) NOT NULL,
	`quantite` INTEGER NOT NULL,
	
	CONSTRAINT pk_denree PRIMARY KEY(id) 
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `sectdenree`
(
	`id_secteur` INTEGER NOT NULL,
	`id_denree` INTEGER NOT NULL,
	`quantite` INTEGER ,

	CONSTRAINT pk_sectdenree PRIMARY KEY(id_secteur,id_denree),
	CONSTRAINT fk_sectdenree_secteur FOREIGN KEY (id_secteur) REFERENCES secteur(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_sectdenree_denree FOREIGN KEY (id_denree) REFERENCES denree(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `deptdenree`
(
	`id_departement` INTEGER NOT NULL,
	`id_denree` INTEGER NOT NULL,
	`quantite` INTEGER ,
	
	CONSTRAINT pk_deptdenree PRIMARY KEY(id_departement,id_denree),
	CONSTRAINT fk_deptdenree_departement FOREIGN KEY (id_departement) REFERENCES departement(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_deptdenree_denree FOREIGN KEY (id_denree) REFERENCES denree(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `regdenree`
(
	`id_region` INTEGER NOT NULL,
	`id_denree` INTEGER NOT NULL,
	`quantite` INTEGER ,
	
	CONSTRAINT pk_regdenree PRIMARY KEY(id_region,id_denree),
	CONSTRAINT fk_regdenree_region FOREIGN KEY (id_region) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_regdenree_denree FOREIGN KEY (id_denree) REFERENCES denree(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

CREATE TABLE `menage_denree`
(
	`id_menage` INTEGER NOT NULL,
	`id_denree` INTEGER NOT NULL,
	`quantite` INTEGER ,
	`id_agent` INTEGER NOT NULL,
	
	CONSTRAINT pk_menagedenree PRIMARY KEY(id_menage,id_denree),
	CONSTRAINT fk_menagedenree_menage FOREIGN KEY (id_menage) REFERENCES menage(id)  ON DELETE CASCADE ON UPDATE CASCADE,
	CONSTRAINT fk_menagedenree_denree FOREIGN KEY (id_denree) REFERENCES denree(id) ON DELETE CASCADE ON UPDATE CASCADE
)
ENGINE=InnoDB
CHARACTER SET utf8
COLLATE utf8_general_ci;

 ALTER TABLE `menage_denree`
ADD CONSTRAINT fk_menagedenree_agent FOREIGN KEY (id_agent) REFERENCES agent(id) ON DELETE CASCADE ON UPDATE CASCADE
     ;